var     http = require('http'),
        sys = require("sys"),
        url = require("url"),
        path = require("path"),
        fs = require("fs");

// for npm, otherwise use require('./path/to/socket.io')
var     io = require('socket.io'),
        server = http.createServer(handleRequest);

server.listen(8070);
console.log('Server running at - ' + server.hostname + ", " + server.port);

function handleRequest(req, response) {
    // your normal server code
    response.writeHead(200, {'Content-Type': 'text/html'});

    fs.readFile('socket_client.html', "binary", function(err, file) {
        if(err) {
            response.sendHeader(500, {"Content-Type": "text/plain"});
            response.write(err + "\n");
            response.close();
            return;
        }

//                response.sendHeader(200);
        response.write(file, "binary");
        response.end();

    });
}

// socket.io 
var socket = io.listen(server);
socket.on('connection', function(client){
    // new client is here!
    client.on('message', function(){ console.log('message received'); })
    client.on('disconnect', function(){ console.log('disconneting'); })
});
